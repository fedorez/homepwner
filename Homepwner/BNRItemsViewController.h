//
//  BNRItemsViewController.h
//  Homepwner
//
//  Created by Denis Fedorets on 22/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BNRItemsViewController : UITableViewController <UIViewControllerRestoration>

@end

NS_ASSUME_NONNULL_END
