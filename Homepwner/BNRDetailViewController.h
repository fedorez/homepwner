//
//  BNRDetailViewController.h
//  Homepwner
//
//  Created by Denis Fedorets on 27/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class BNRItem;

@interface BNRDetailViewController : UIViewController <UIViewControllerRestoration>

- (instancetype)initForNewItem:(BOOL)isNew;

@property (nonatomic, strong) BNRItem *item;
@property (nonatomic, copy) void (^dismissBlock)(void);

@end

NS_ASSUME_NONNULL_END
