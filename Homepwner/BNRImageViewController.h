//
//  BNRImageViewController.h
//  Homepwner
//
//  Created by Denis Fedorets on 24/02/2019.
//  Copyright © 2019 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BNRImageViewController : UIViewController

@property (nonatomic, strong) UIImage *image;

@end

NS_ASSUME_NONNULL_END
