//
//  BNRImageTransformer.m
//  Homepwner
//
//  Created by Denis Fedorets on 10/03/2019.
//  Copyright © 2019 Denis Fedorets. All rights reserved.
//

#import "BNRImageTransformer.h"
#import <UIKit/UIKit.h>

@implementation BNRImageTransformer

+ (Class)transformedValueClass
{
    return [NSData class];
}

- (id)transformedValue:(id)value
{
    if(!value) {
        return nil;
    }
    
    if([value isKindOfClass:[NSData class]]) {
        return value;
    }
    
    return UIImagePNGRepresentation(value);
}

- (id)reverseTransformedValue:(id)value
{
    return [UIImage imageWithData:value];
}

@end
