//
//  BNRItemStore.h
//  Homepwner
//
//  Created by Denis Fedorets on 22/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNRItem;

NS_ASSUME_NONNULL_BEGIN

@interface BNRItemStore : NSObject

@property (nonatomic, readonly) NSArray *allItems;

+ (instancetype)sharedStore;
- (BNRItem *)createItem;

- (void)removeItem:(BNRItem *)item;

- (void)moveItemAtIndex:(NSUInteger)fromindex
                toindex:(NSUInteger)toIndex;

- (BOOL)saveChanges;
- (NSString *)itemArchivePath;
- (NSArray *)allAssetTypes;


@end

NS_ASSUME_NONNULL_END
