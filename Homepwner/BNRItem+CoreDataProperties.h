//
//  BNRItem+CoreDataProperties.h
//  Homepwner
//
//  Created by Denis Fedorets on 10/03/2019.
//  Copyright © 2019 Denis Fedorets. All rights reserved.
//
//

#import "BNRItem+CoreDataClass.h"
#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface BNRItem (CoreDataProperties)

+ (NSFetchRequest<BNRItem *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *itemName;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nonatomic) int valueInDollars;
@property (nullable, nonatomic, copy) NSDate *dateCreated;
@property (nullable, nonatomic, copy) NSString *itemKey;
@property (nullable, nonatomic, retain) UIImage *thumbnail;
@property (nonatomic) double orderingValue;
@property (nullable, nonatomic, retain) NSManagedObject *assetType;

- (void)setThumbnailFromImage:(UIImage *)image;
- (void)awakeFromInsert;
- (instancetype)initWithItemName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
