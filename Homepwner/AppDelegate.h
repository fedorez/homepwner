//
//  AppDelegate.h
//  Homepwner
//
//  Created by Denis Fedorets on 22/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const BNRNextItemValuePrefsKey;
extern NSString * const BNRNextItemNamePrefsKey;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

