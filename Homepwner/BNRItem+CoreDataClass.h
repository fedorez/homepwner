//
//  BNRItem+CoreDataClass.h
//  Homepwner
//
//  Created by Denis Fedorets on 10/03/2019.
//  Copyright © 2019 Denis Fedorets. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BNRAssetType, NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface BNRItem : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BNRItem+CoreDataProperties.h"
