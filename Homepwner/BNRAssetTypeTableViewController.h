//
//  BNRAssetTypeTableViewController.h
//  Homepwner
//
//  Created by Denis Fedorets on 12/03/2019.
//  Copyright © 2019 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNRItem;

NS_ASSUME_NONNULL_BEGIN

@interface BNRAssetTypeTableViewController : UITableViewController

@property (nonatomic, strong) BNRItem *item;

@end

NS_ASSUME_NONNULL_END
